#include <LiquidCrystal.h>

// Consts //

// Voltages
const float MAXV = 7.5;     // display a warning if we exceed this voltage
const float MINV = 1.2;     // don't display anything under this voltage except for 0

// Resistors
const float R1 = 9920.0;    // first resistor value in ohms
const float R2 = 5070.0;    // second resistor value in ohms

// Pins
const byte Btn = 7;         // button used to trigger additional functions
const byte Buz = 8;         // active buzzer pin beep on notification

// Logic
const byte Lng = 100;       // long click duration in centiseconds
const byte Aln = 20;        // long click during alarm set
const byte Inc = 10;        // fast increase rate
const byte Cng = 2;         // seconds without clicks to skip to the next set time
const byte Dly = 10;        // standard delay
const byte Set = 2;         // wait to settle voltage reference in millisecs

// Libs //
LiquidCrystal lcd(11, 10, 5, 4, 3, 2);

// Vars //
byte cents;                 // centieconds (0.01s)
byte secs;                  // seconds
byte mins;                  // minutes
byte hours;                 // hours

byte tState;                // are we setting seconds (0), minutes (1) or hours?
byte tDur;                  // time since last click while setting the alarm
byte tDelay;                // currently used delay

byte aSecs;                 // seconds before the alarm
byte aMins;                 // minutes before the alarm
byte aHours;                // hours before the alarm

byte cTime;                 // click time

bool alarm;                 // is the alarm set?
bool released;              // was the button released?
bool clicked;               // was the button clicked?
bool shouldBeep;            // should we beep?
bool usedFI;                // used fast increase?
bool beep;                  // are we beeping?
bool showTime;              // should we show current operating time or alarm?

void setup()                                                                                     
{
   pinMode(A2,  INPUT);
   pinMode(Btn, INPUT);
   pinMode(Buz, OUTPUT);
   lcd.begin(16,2);
   showTime = true;
}

void loop()
{
   float aValue;
   float ref;
   float voltage;

   // called once per second
   if (cents == 0)
   {
      aValue = analogRead(A2);
      tDelay = Dly - Set;

      lcd.clear();

      lcd.print("Voltage: ");

      ref = read_vcc() / 1000.0;

      // debug
      //lcd.setCursor(0,1);
      //lcd.print("Raw: ");
      //lcd.setCursor(6,1);
      //lcd.print(int(aValue));

      // toggle between hours, minutes and seconds

      if (!showTime)
      {
         if (!clicked)
         {
            tDur++;

            usedFI = false;
         
            if (tDur >= Cng)
            {
               tDur = 0;
               tState++;

               if (tState >= 4)
               {
                  tState   = 0;
                  showTime = true;

                  if (aHours > 0 || aMins > 0 || aSecs > 0)
                  {
                     shouldBeep = false;
                     tState     = 0;
                     cTime      = 0;
                     alarm      = true;
                  }
               }
            }
         }

         if (!released && cTime >= Aln)
         {
            if (tState == 0)
               tState = 1;

            usedFI  = true;
            clicked = true;
            set_alarm(Inc);
         }

         else
            clicked = false;
      }

      if (showTime)
         print_time();

      else
         print_set_menu();

      lcd.setCursor(11,0);

      voltage = aValue * ref / 1024.0;
      voltage = voltage / (R2 / (R1 + R2));

      if (voltage > 0.5)
         lcd.print(max(MINV, voltage));

      else
         lcd.print(0.0);

      lcd.setCursor(15,0);
      lcd.print("v");

      if (voltage >= MAXV)
      {
         lcd.setCursor(12,1);
         lcd.print("HIGH");
      }

      if (shouldBeep)
      {
         if (beep)
         {
            beep = false;
            digitalWrite(Buz, LOW);
         }

         else
         {
            beep = true;
            digitalWrite(Buz, HIGH);
         }
      }

      else
      {
         beep = false;
         digitalWrite(Buz, LOW);
      }
   }

   else
      tDelay = Dly;

   cents++;

   if (cents > 99)
   {
      cents = 0;
      secs++;
   }

   if (!alarm)
   {
      if (digitalRead(Btn) == HIGH)
      {
         if (released)
         {
            if (!showTime) 
               clicked    = true;

            cTime      = 0;
            usedFI     = false;
            released   = false;
            showTime   = false;
            tDur       = -1;
         }

         else if (cTime < Aln)
            cTime += 1;
      }

      else 
      {
         // setting the alarm
         if (!released && !showTime)
         {
            if (tState == 0)
               tState = 1;

            else if (!usedFI)
               set_alarm(1);
         }

         released = true;
      }
   }

   // deactivate the alarm after a 2 seconds press
   else
   {
      if (digitalRead(Btn) == HIGH)
      {
         released = false;
         cTime++;

         if (shouldBeep)
         {
            shouldBeep = false;
            alarm      = false;
         }
      }

      else
      {
         released = true;
         cTime = 0;
      }

      if (cTime >= Lng)
      {
         aHours   = 0;
         aMins    = 0;
         aSecs    = 0;
         alarm    = false;
         showTime = true;
         cTime    = 0;
      }
   }

   delay(tDelay);
}

// print time from startup
void print_time()
{
   byte printScs, printMns, printHrs;

   lcd.setCursor(0,1);

   if (secs > 59)
   {
      secs = 0; 
      mins++;
   }

   if (mins > 59)
   {
      mins = 0;
      hours++;

      if (hours > 99)
         hours = 0;
   }

   if (alarm)
   {
      if (aSecs > 0)
         aSecs--;

      else if (aMins > 0)
      {
         aMins--;
         aSecs = 59;
      }

      else if (aHours > 0)
      {
         aHours--;
         aSecs = 59;
         aMins = 59;
      }

      else
      {
         shouldBeep = true;
      }

      printScs = aSecs;
      printMns = aMins;
      printHrs = aHours;
   }

   else
   {
      printScs = secs;
      printMns = mins;
      printHrs = hours;
   }

   // zfill hours
   if (printHrs < 10)
   {
      lcd.print(0);
      lcd.setCursor(1,1);
   }

   // print hours
   lcd.print(printHrs);

   lcd.setCursor(2, 1);
   lcd.print(":");
   lcd.setCursor(3, 1);

   // zfill mins
   if (printMns < 10)
   {
      lcd.print(0);
      lcd.setCursor(4,1);
   }

   // print mins
   lcd.print(printMns);

   lcd.setCursor(5, 1);
   lcd.print(":");
   lcd.setCursor(6, 1);

   // zfill secs
   if (printScs < 10)
   {
      lcd.print(0);
      lcd.setCursor(7,1);
   }

   // print secs
   lcd.print(printScs);

   if (alarm)
   {
      lcd.setCursor(9,1);
      lcd.print("A");
   }
}

// print menu to set alarm
void print_set_menu()
{
   lcd.setCursor(0,1);
   
   // zfill hours
   if (aHours < 10)
   {
      lcd.print(0);
      lcd.setCursor(1,1);
   }

   // print hours
   lcd.print(aHours);

   lcd.setCursor(2, 1);
   lcd.print(":");
   lcd.setCursor(3, 1);

   if (tState > 1)
   {
      // zfill mins
      if (aMins < 10)
      {
         lcd.print(0);
         lcd.setCursor(4,1);
      }

      // print mins
      lcd.print(aMins);
   }

   else
      lcd.print("__");

   lcd.setCursor(5, 1);
   lcd.print(":");
   lcd.setCursor(6, 1);

   if (tState > 2)
   {
      // zfill secs
      if (aSecs < 10)
      {
         lcd.print(0);
         lcd.setCursor(7,1);
      }

      // print secs
      lcd.print(aSecs);
   }

   else
      lcd.print("__");
}

// set alarm time using a specified increase
void set_alarm(byte increase)
{
   if (tState == 1)
   {
      aHours += increase;

      if (aHours > 59)
         aHours = 0;
   }

   else if (tState == 2)
   {
      aMins += increase; 

      if (aMins > 59)
         aMins = 0;
   }

   else if (tState == 3)
   {
      aSecs += increase; 

      if (aSecs > 59)
         aSecs = 0;
   }
}

// get proper voltage reference
long read_vcc()
{
   // Read 1.1V reference against AVcc
   // set the reference to Vcc and the measurement to the internal 1.1V reference
   #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
      ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
   #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
      ADMUX = _BV(MUX5) | _BV(MUX0);
   #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
      ADMUX = _BV(MUX3) | _BV(MUX2);
   #else
      ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
   #endif  

   delay(2); // Wait for Vref to settle
   ADCSRA |= _BV(ADSC); // Start conversion
   while (bit_is_set(ADCSRA,ADSC)); // measuring

   uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
   uint8_t high = ADCH; // unlocks both

   long result = (high << 8) | low;

   result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
   return result;
}
